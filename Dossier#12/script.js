// Obligatoire pour Codesandbox (pb de dépendances...)
                    // Le fichier script.js est donc ici inutile...
                    // Le script est placé à la fin du body car l'attribut defer ne fonctionne que pour des scripts externes
                    // CE N'EST PAS UNE BONNE PRATIQUE !!!
              
                    // Variable globale contenant l'état du lecteur
                    let etatLecteur;
              
                    function lecteurPret(event) {
                      // event.target = lecteur
                      event.target.setVolume(50);
                    }
              
                    function changementLecteur(event) {
                      // event.data = état du lecteur
                      etatLecteur = event.data;
                    }
              
                    let lecteur;
              
                    function onYouTubeIframeAPIReady() {
                      lecteur = new YT.Player("video", {
                        height: "390",
                        width: "640",
                        videoId: "3dKILIX_3mw",
                        playerVars: {
                          color: "white",
                          enablejsapi: 1,
                          modestbranding: 1,
                          rel: 0
                        },
                        events: {
                          onReady: lecteurPret,
                          onStateChange: changementLecteur
                        }
                      });
                    }
                        const hauteurVideo = $("#video").height();
                            // Position Y de la vidéo
                        const posYVideo = $("#video").offset().top;
                        // Valeur declenchant la modification de l'affichage (choix "esthétique")
                        const seuil = posYVideo + 0.75 * hauteurVideo;

                        // Gestion du défilement
                        $(window).scroll(function () {
                            // Récupération de la valeur du défilement vertical
                            const scroll = $(window).scrollTop();

                            // Classe permettant l'exécution du CSS
                            $("#video").toggleClass(
                            "scroll",
                            etatLecteur === YT.PlayerState.PLAYING && scroll > seuil
                            );
                        });

                        // Menu

                        const btnMenu = document.querySelector('body>label');

                        btnMenu.addEventListener('click', () => {
                          btnMenu.classList.toggle('opened');
                        });




                      // caroussel spécialités

                      // Variable globale
                      let indexa = 0;
                    
                      // Gestion des événements
                      $('.span_specialite>span').click(function () {
                      // Récupération index
                      let indexN = $('.span_specialite>span').index(this);
                    
                      // Renouveller l'image
                      $('#specialite>article').eq(indexa).fadeOut(1000).end().eq(indexN).fadeIn(1000);
                    
                      // Mettre à jour l'index
                      indexa = indexN;
                    });

                    // caroussel vidéo et images

                    // Variable globale
                    let indexb = 0;
                    
                    // Gestion des événements
                    $('.span_video_et_images>span').click(function () {
                    // Récupération index
                    let indexN = $('.span_video_et_images>span').index(this);
                  
                    // Renouveller l'image
                    $('#imagesetvideos>.imgvideos>*').eq(indexb).fadeOut(1000).end().eq(indexN).fadeIn(1000);
                  
                    // Mettre à jour l'index
                    indexb = indexN;
                  });


                      // CARTOGRAPHIE (SLIDE 5)

                      // Création de la carte, vide à ce stade
                      const carte = L.map('carte', {
                        center: [47.2608333, 2.4188888888888886], // Centre de la France
                        zoom: 5,
                        minZoom: 4,
                        maxZoom: 19,
                      });

                      // Ajout des tuiles (ici OpenStreetMap)
                      // https://wiki.openstreetmap.org/wiki/Tiles#Servers
                      L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
                      }).addTo(carte);

                      // Ajout de l'échelle
                      L.control.scale().addTo(carte);

                      // Appelée si récupération des coordonnées réussie

                        // Injection du résultat dans du texte
                      function positionSucces(position) {
                        const lat = Math.round(1000 * position.coords.latitude) / 1000;
                        const long = Math.round(1000 * position.coords.longitude) / 1000;
                        $("#localisation").text(`Latitude: ${lat}°, Longitude: ${long}°`);
                        carte.flyTo([lat, long], 12);
                      };

                      // Appelée si échec de récuparation des coordonnées
                      function positionErreur(erreurPosition) {
                        // Cas d'usage du switch !
                        let natureErreur;
                        switch (erreurPosition.code) {
                          case erreurPosition.TIMEOUT:
                            // Attention, durée par défaut de récupération des coordonnées infini
                            natureErreur = "La géolocalisation prends trop de temps...";
                            break;
                          case erreurPosition.PERMISSION_DENIED:
                            natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
                            break;
                          case erreurPosition.POSITION_UNAVAILABLE:
                            natureErreur = "Votre position n'a pu être déterminée.";
                            break;
                          default:
                            natureErreur = "Une erreur inattendue s'est produite.";
                        }
                        // Injection du texte
                        $("#localisation").text(natureErreur);
                      };

                      // Récupération des coordonnées au clic sur le bouton
                      $("#btnLoc").click(function() {
                        // Support de la géolocalisation
                        if ("geolocation" in navigator) {
                          // Support = exécution du callback selon le résultat
                          navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
                            enableHighAccuracy: true,
                            timeout: 5000,
                            maximumAge: 30000,
                          });
                        } else {
                          // Non support = injection de texte
                          $("#localisation").text("La géolocalisation n'est pas supportée par votre navigateur");
                        }
                      });
